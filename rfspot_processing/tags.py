"""
Tags module

"""
from . import sql_rows
import aiohttp
import asyncio
import os.path
import logging

logger = logging.getLogger(__name__)

class TagsException(Exception):
    pass


@asyncio.coroutine
def _pass_attributes(id_pass, maplayer_url, token=None):
    """
    Returns attributes on any given pass by querying ArcGIS server.

    :param id_pass: The id_pass value for any given pass
    :param maplayer_url: URL to the REST endpoint of a pass layer in ArcGIS
                         server
    :param token: Session token for ArcGIS server

    """
    request = 'query'
    url = '/'.join((maplayer_url, request))
    params = {'where': 'id_pass={0}'.format(id_pass),
              'outFields': 'id_org,id_campus,id_store,id_structure,id_floor,'
                           'id_pass',
              'f': 'json'}
    if token:
        params['token'] = token
    session = aiohttp.ClientSession()
    try:
        logger.debug('url={0} data={1}'.format(url, params))
        response = yield from session.post(url=url, data=params)
        assert response.status == 200
        result = yield from response.json()
        session.close()  # TODO: Does this belong here?
        features = result.get('features')
        if not features:
            raise TagsException('Pass {0} not found in product DB'.format(id_pass))
        return features[0].get('attributes')
    except Exception as e:
        raise TagsException('Product DB error (pass {0})'.format(id_pass)) from e
    finally:
        session.close()


@asyncio.coroutine
def tag_entries(id_pass, woodbeck_url, woodbeck_api_key, maplayer_url,
                token=None, results={}):
    """
    Returns pass entries from Kris Woodbeck's API.

    :param id_pass: The id_pass value for any given pass
    :param woodbeck_url: Base URL to Kris Woodbeck's API
    :param woodbeck_api_key: Kris Woodbeck's API key
    :param maplayer_url: URL to the REST endpoint of a pass layer in ArcGIS
                         server
    :param token: Session token for ArcGIS server

    """
    pass_attributes = yield from _pass_attributes(id_pass=id_pass,
                                                  maplayer_url=maplayer_url,
                                                  token=token)
    session = aiohttp.ClientSession()
    
    # First try Kris Woodbeck's outofstock API.
    try:
        attributes = {}
        attributes['org'] = pass_attributes.get('id_org')
        attributes['campus'] = pass_attributes.get('id_campus')
        attributes['store'] = pass_attributes.get('id_store')
        attributes['structure'] = pass_attributes.get('id_structure')
        attributes['floor'] = pass_attributes.get('id_floor')
        attributes['pass'] = pass_attributes.get('id_pass')
        attributes['key'] = woodbeck_api_key
        url = '/'.join((woodbeck_url, 'outofstock/'))
        logger.debug('url={0} data={1}'.format(url, attributes))
        response = yield from session.post(url=url, data=attributes)
        assert response.status == 200
        result = yield from response.json()
        for item in result:
            item.update(pass_attributes)
        results[id_pass] = result

    except Exception as e:
        raise TagsException('Kris Woodbeck API error (pass {0})'.format(id_pass)) from e

    finally:
        session.close()


def kris_woodbeck_image_filenames(loop, wb_cfg, tag_lyr_cfg, passes=[]):
    """
    Returns a dictionary containing sets of image file names that have been
    processed by Kris Woodbeck's tag processing system.

    :param loop: Application's asyncio event loop
    :param wb_cfg: Configuration dictionary for Kris Woodbeck's API
    :param tag_lyr_cfg: Configuration dictionary for an ArcGIS tag layer
    :param passes: A list of passes to check

    """
    wresult = {}
    tasks = [tag_entries(id_pass=p, woodbeck_url=wb_cfg.get('url'),
                         woodbeck_api_key=wb_cfg.get('key'),
                         maplayer_url=tag_lyr_cfg.get('url'),
                         token=tag_lyr_cfg.get('token'),
                         results=wresult) for p in passes]
    loop.run_until_complete(asyncio.wait(tasks))

    # Weed out non-image filename data
    results = {}
    for p in passes:
        imgs = []
        if p in wresult:
            for wb in wresult.get(p):
                try:
                    imgs.append(wb.get('filename_image'))
                except AttributeError:
                    # TODO: Should we raise an exception?
                    logger.error('Kris Woodbeck API Error with pass {0}, '
                                 'content: {1}'.format(p, wb))
        results[p] = set([i.replace('retina.autops.', '') for i in imgs])
    return results


def prc_db_image_filenames(db_adapter, prc_db_cfg, passes=[]):
    """
    Returns a dictionary of image filenames per pass id.

    :param db_adapter: A database adapter, e.g. ``psycopg2``
    :param prc_db_cfg: Configuration dictionary for the processing DB
    :param passes: A list of pass IDs
    """
    try:
        sqlpath = os.path.sep.join((os.path.dirname(__file__),
                                    'data', 'sql_templates',
                                    'tags_filenames_from_processing_db.sql'))
        with open(sqlpath, 'r') as sqlfile:
            sqltext = sqlfile.read()
        results = {}
        dbresults = {}
        dsn = prc_db_cfg.get('dsn')
        sql = sqltext.format(','.join([str(p) for p in passes]))
        for row in sql_rows(adapter=db_adapter, dsn=dsn, sql=sql):
            if row[0] in dbresults:
                dbresults[row[0]].append(row[1])
            else:
                dbresults[row[0]] = [row[1]]
        
        for p in passes:
            results[p] = set(dbresults.get(p))

        return results
    except Exception as e:
        raise TagsException('Error processing image filenames from processing database.') from e


def determine_passes(db_adapter, prc_db_cfg):
    """
    Determines passes that need to be processed by querying the processing
    database.

    :param db_adapter: A database adapter, e.g. ``psycopg2``
    :param prc_db_config: Configuration dictionary for the processing DB

    """
    try:
        sqlpath = os.path.sep.join((os.path.dirname(__file__),
                                    'data', 'sql_templates',
                                    'tags_auto_determine_passes.sql'))
        with open(sqlpath, 'r') as sqlfile:
            sqltext = sqlfile.read()
        rows = sql_rows(adapter=db_adapter, dsn=prc_db_cfg.get('dsn'),
                        sql=sqltext)
        passes = [row[0] for row in rows]
        return passes
    except Exception as e:
        raise TagsException('Error determining passes from processing database.') from e


def refresh_tag_table(loop, db_adapter, prc_db_cfg, wb_cfg, tag_lyr_cfg,
                      passes=[]):
    """
    Clears out current tag data by pass(es) and inserts new records from Kris
    Woodbeck's system.

    :param loop: Application's asyncio event loop
    :param db_adapter: A database adapter, e.g. ``psycopg2``
    :param prc_db_cfg: Configuration dictionary for the processing DB
    :param wb_cfg: Configuration dictionary for Kris Woodbeck's API
    :param tag_lyr_cfg: Configuration dictionary for an ArcGIS tag layer
    :param passes: An iterable collection of passes.

    """
    wresult = {}
    tasks = [tag_entries(id_pass=p, woodbeck_url=wb_cfg.get('url'),
                         woodbeck_api_key=wb_cfg.get('key'),
                         maplayer_url=tag_lyr_cfg.get('url'),
                         token=tag_lyr_cfg.get('token'),
                         results=wresult) for p in passes]
    loop.run_until_complete(asyncio.wait(tasks))

    sqlpath = os.path.sep.join((os.path.dirname(__file__),
                                'data', 'sql_templates',
                                'delete_tag_data.sql'))
    with open(sqlpath, 'r') as sqlfile:
        deltagssql = sqlfile.read()
        deltagssql = deltagssql.format(','.join([str(p) for p in passes]))

    sqlpath = os.path.sep.join((os.path.dirname(__file__),
                                'data', 'sql_templates',
                                'insert_tag_data.sql'))
    with open(sqlpath, 'r') as sqlfile:
        inserttagsql = sqlfile.read()
    
    sqlparts = [deltagssql]
    for p in passes:
        if p in wresult:
            for item in wresult[p]:
                sqlparts.append(inserttagsql.format(**item))

    sqltransaction = ''.join(sqlparts)
    sqltext = 'BEGIN;\n{0}\nCOMMIT;'.format(sqltransaction)
    rows = sql_rows(adapter=db_adapter, dsn=prc_db_cfg.get('dsn'),
                    sql=sqltext)


def filter_passes(passes, db_imgs, wb_imgs):
    """
    Filters out passes based on a difference test of image filenames.

    :param passes: List of passes
    :param db_imgs: Set of image filenames from processing database
    :param wb_imgs: Set of image filenames from Kris Woodbeck's system

    """
    for p in list(passes):
        diff = db_imgs.get(p).difference(wb_imgs.get(p))
        try:
            if diff:
                passes.remove(p)
                raise TagsException
        except TagsException:
            imgs = ', '.join([img for img in diff])
            logger.warning('Pass {0} missing images: {1} in Kris '
                           'Woodbeck\'s API'.format(p, imgs))
            continue
    return passes


def update_tags_nearest_barcode(db_adapter, prc_db_cfg, passes=[]):
    """
    Updates the tags table with the id_barcode value of the nearest barcode.

    :param db_adapter: A database adapter, e.g. ``psycopg2``
    :param prc_db_cfg: Configuration dictionary for the processing DB
    :param passes: An iterable collection of passes.

    """
    sqlpath = os.path.sep.join((os.path.dirname(__file__),
                                'data', 'sql_templates',
                                'update_tags_nearest_barcode.sql'))

    with open(sqlpath, 'r') as sqlfile:
        sqltext = sqlfile.read()
        sqltext = sqltext.format(','.join([str(p) for p in passes]))

    rows = sql_rows(adapter=db_adapter, dsn=prc_db_cfg.get('dsn'),
                    sql=sqltext)
