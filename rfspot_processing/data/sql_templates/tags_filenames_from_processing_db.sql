/*
Returns image filenames for a specific pass from the processing database.
*/
SELECT id_pass, filename
FROM file_status
WHERE  id_pass IN ({0})
   AND file_type=1;
