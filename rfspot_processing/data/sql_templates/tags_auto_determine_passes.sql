/*
This query selects id_pass values from the proc_pass table that are ready to
be processed. 
*/
SELECT id_pass FROM proc_pass WHERE proc_bay=1 AND proc_tag_seq=0;
