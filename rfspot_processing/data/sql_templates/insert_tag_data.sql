/*
Inserts a new tag into processing database. id_tag_type is hardcoded for now.
*/
INSERT INTO tag (id_org, id_pass, id_tag_type, id_tag_size,
	         in_stock, in_stock_confidence,
		 x_tag_ll_image, y_tag_ll_image,
		 x_tag_ur_image, y_tag_ur_image,
	         filename_image)
VALUES ({id_org}, {id_pass}, 1, {id_tag_size},
	         {in_stock}, {in_stock_confidence},
		 {x_tag_ll_image}, {y_tag_ll_image},
		 {x_tag_ur_image}, {y_tag_ur_image},
	         {filename_image});
