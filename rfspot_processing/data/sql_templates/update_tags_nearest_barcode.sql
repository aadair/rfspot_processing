/*
This update function will set the id_barcode value of any given tag to its
nearest valid barcode.

Valid barcodes start with a 9807 or 9809 prefix.
4928 is derived from the image resolution.
PostGIS must be enabled on the database.

This is not perfect in cases where barcode data is really terrible.
*/
BEGIN;
UPDATE tag SET id_barcode = -1 WHERE id_pass in ({0});
UPDATE tag SET id_barcode = sub.id_barcode
FROM (SELECT DISTINCT ON(t.id_tag) t.id_pass, t.filename_image,
  t.id_tag, b.id_barcode
  FROM barcode b
  JOIN tag t
  ON b.id_pass = t.id_pass AND b.filename_image = t.filename_image
  WHERE t.id_pass IN ({0}) AND (b.barcode LIKE '9807%' OR b.barcode LIKE '9809%')
  ORDER BY t.id_tag, ST_Distance(ST_MakeBox2D(ST_MakePoint(
	   t.x_tag_ll_image, -t.y_tag_ll_image),
	   ST_MakePoint(t.x_tag_ur_image, -t.y_tag_ur_image)),
	   ST_MakePoint(b.image_y, -(4928 - b.image_x)))) sub
WHERE tag.id_tag = sub.id_tag;
COMMIT;
