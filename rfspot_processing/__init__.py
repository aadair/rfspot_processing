"""
rfspot_processing module

"""
__version__ = '0.0.1'

import importlib


def sql_rows(adapter, dsn, sql):
    """
    Runs SQL and returns a selection set from a DB provider.

    This is a generator function and should be used in place of
    ``select_all_rows()`` whenever possible.

    :param adapter: Database adapter object, e.g. psycopg2
    :param dsn: Database connection string
    :param sql: SQL query string

    """
    adapter = importlib.import_module(adapter)
    with adapter.connect(dsn) as conn:
        with conn.cursor() as cur:
            cur.execute(sql)
            while True:
                row = cur.fetchone()
                if row:
                    yield row
                else:
                    break


def _sql_all_rows(adapter, dsn, sql):
    """
    Returns a selection set from a DB provider.

    This function is dangerous to use because we do not know the size of the
    data being returned. Use with caution.

    :param adapter: Database adapter object, e.g. psycopg2
    :param dsn: Database connection string
    :param sql: SQL query string

    """
    adapter = importlib.import_module(adapter)
    with adapter.connect(dsn) as conn:
        with conn.cursor() as cur:
            cur.execute(sql)
            return cur.fetchall()
