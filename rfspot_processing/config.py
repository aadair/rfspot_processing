"""
Configuration module.

"""
import os.path
import toml


def load_config(conf_name):
    """
    Returns the contents of a TOML file as a dictionary.

    """
    home = os.path.expanduser('~')
    path = os.path.join(home, '.rfspot', conf_name)
    try:
        with open(path, 'r') as config_file:
            cfg = toml.loads(config_file.read())
        return cfg
    except:
        raise Exception('Unable to load configuration: {0}'.format(path))
