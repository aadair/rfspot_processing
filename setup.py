#!/usr/bin/env python3
"""
Setup.py script for rfspot_processing

"""
from setuptools import setup, find_packages
import rfspot_processing


def readme(filepath):
    """
    Returns contents of a readme file as a single string.

    """
    with open(filepath, 'r') as rm:
        return rm.read()

requires = ['aiohttp', 'psycopg2', 'toml']
setup_options = dict(name='rfspot_processing',
                     version=rfspot_processing.__version__,
                     description='RFSpot processing distribution and scripts',
                     license='Other/Proprietary License',
                     long_description=readme('README.rst'),
                     author='Allan Adair',
                     author_email='allan@rfspot.com',
                     url='https://github.com/RFSpot/services',
                     scripts=['scripts/process_tags_and_barcodes.py'],
                     packages=find_packages('.'),
                     package_dir={'rfspot_processing': 'rfspot_processing'},
                     package_data={'rfspot_processing': ['data/sql_templates/*']},
                     install_requires=requires,
                     classifiers=('Development Status :: 1 - Planning',
                                  'Intended Audience :: Developers',
                                  'Intended Audience :: System Administrators',
                                  'Natural Language :: English',
                                  'License :: Other/Proprietary License',
                                  'Programming Language :: Python',
                                  'Programming Language :: Python :: 3.4'))
setup(**setup_options)
