#!/usr/bin/env python3
"""
Utility script for processing tags.

"""
import argparse
import asyncio
import logging
import os.path
from rfspot_processing.config import load_config
from rfspot_processing.tags import determine_passes, \
    filter_passes, kris_woodbeck_image_filenames, prc_db_image_filenames, \
    refresh_tag_table, update_tags_nearest_barcode

if __name__ == '__main__':
    # Script logger.
    logger = logging.getLogger(os.path.basename(__file__))

    # Get the tags logger.
    tags_logger = logging.getLogger('rfspot_processing.tags')

    # Suppress aiohttp warnings. Both the ArcGIS server REST API and Kris
    # Woodbeck's API send incorrect MIME types on json requests. aiohttp warns
    # about this by default and it writes to STDOUT. Very annoying.
    logging.getLogger('aiohttp').setLevel(logging.ERROR)

    # Create formatter.
    formatter = logging.Formatter('{levelname}, {name}, {message}', style='{')

    # Parse arguments
    # TODO: Do we want to add an argument for specifying custom config?
    parser = argparse.ArgumentParser(description=__doc__)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--passes', '-p', type=int, nargs='+',
                       help='Specify specific passes by ID')
    group.add_argument('--auto', '-a', action='store_true',
                       help='Automatically determine for which passes to process tags')
    parser.add_argument('--verbose', '-v', action='store_true',
                        help='Output verbose messages to STDOUT')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='Set logging level to logging.DEBUG')
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    if args.verbose:
        # Create console handler and set level.
        ch = logging.StreamHandler()
        ch.setLevel(logger.level)

        # Add formatter to ch.
        ch.setFormatter(formatter)

        # Add ch to loggers.
        logger.addHandler(ch)
        tags_logger.addHandler(ch)

    try:
        # Get the asyncio event loop
        loop = asyncio.get_event_loop()

        # Gather configuration settings from ~/.rfspot/tags.conf.
        #
        # See README.rst for examples on how to format this file.
        #
        # Sensitive data is stored in this file, so file permissions should be set
        # to 700 and 600 for .rfspot/ and tags.conf, respectively.
        cfg = load_config('tags.conf')

        # These are configuration dictionaries specific to this script and
        # tags.conf.
        prc_db_cfg = cfg.get('processingdb')
        tag_lyr_cfg = cfg.get('tags_arcgis_map_layer')
        wb_cfg = cfg.get('kriswoodbeckapi')

        if args.passes:
            passes = args.passes
        else:
            # Automatically determine passes by querying proc_pass table.
            passes = determine_passes(db_adapter='psycopg2',
                                      prc_db_cfg=prc_db_cfg)

        # Get image filenames from Kris Woodbeck's API.
        wb_imgs = kris_woodbeck_image_filenames(loop=loop, wb_cfg=wb_cfg,
                                                tag_lyr_cfg=tag_lyr_cfg,
                                                passes=passes)

        # Get image filenames from processing database.
        db_imgs = prc_db_image_filenames(db_adapter='psycopg2',
                                         prc_db_cfg=prc_db_cfg, passes=passes)

        # Filter out passes that have not been fully processed by Kris's
        # system.
        passes = filter_passes(passes=passes, db_imgs=db_imgs, wb_imgs=wb_imgs)
        logger.debug('Passes: {0}'.format(', '.join([str(p) for p in passes])))

        # Clear out old tags and insert new tags (by pass id) from Kris's system.
        logger.debug('Clearing old tag data and inserting new tag data.')
        rows = refresh_tag_table(loop=loop, db_adapter='psycopg2',
                                 prc_db_cfg=prc_db_cfg, wb_cfg=wb_cfg,
                                 tag_lyr_cfg=tag_lyr_cfg, passes=passes)

        # Update tags with information from nearest barcodes.
        logger.debug('Updating tags with information from nearest barcodes.')
        rows = update_tags_nearest_barcode(db_adapter='psycopg2',
                                           prc_db_cfg=prc_db_cfg,
                                           passes=passes)

    except Exception as e:
        logger.critical(e)

    finally:
        # Close the asyncio event loop
        loop.close()
